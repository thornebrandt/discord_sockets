import WebSocket from "ws";
import ENV from "./config.json" assert { type: "json" };

const initialURL = "wss://gateway.discord.gg";
let url = initialURL,
    sessionId = "";
let ws;
let interval = 0,
    seq = -1;

let payload = {
    op: 2,
    d: {
        token: ENV.GATEWAY_TOKEN,
        intents: 33280,
        properties: {
            $os: "linux",
            $browser: "chrome",
            $device: "desktop",
        },
    },
};

const servers = ENV.servers;

const heartbeat = (ms) => {
    return setInterval(() => {
        ws.send(JSON.stringify({ op: 1, d: null }));
    }, ms);
}

const initializeWebSocket = () => {
    if (ws && ws.readyState !== -3) ws.close();
    let wasReady = false;
    ws = new WebSocket(url + "/?v=10&encoding=json");

    ws.on("open", () => {
        if (url != initialURL) {
            const resumePayload = {
                op: 6,
                d: {
                    token: ENV.GATEWAY_TOKEN,
                    sessionId,
                    seq: seq,
                },
            };
            ws.send(JSON.stringify(resumePayload));
        }
    });

    ws.on("error", (err) => {
        console.log(err);
    });

    ws.on("close", () => {
        if (wasReady) {
            console.log("Connection closed. Reconnecting...");
            setTimeout(() => {
                initializeWebSocket();
            }, 2500);
        }
    });

    ws.on("message", (data) => {
        let message_payload = JSON.parse(data);
        const { t, d, s, op } = message_payload;
        switch (op) {
            case 10:
                const { heartbeat_interval } = d;
                interval = heartbeat(heartbeat_interval);
                wasReady = true;
                if (url === initialURL) ws.send(JSON.stringify(payload));
                break;
            case 0:
                seq = s;
                break;
        }

        switch (t) {
            case "READY":
                console.log("Gateway connection ready.");
                url = d.resume_gateway_url;
                sessionId = d.session_id;
                break;

            case "RESUMED":
                console.log("Gateway connection resumed!");
                break;

            case "MESSAGE_CREATE":

                for (let server of servers) {
                    if (server == d.guild_id) {
                        let author = d.author.username;
                        // let disc = d.author.discriminator;
                        // let channel = d.channel_id;
                        let content = d.content;
                        console.log(author + " : " + content);
                        break;
                    }
                }
                break;
        }
    });
}

initializeWebSocket();